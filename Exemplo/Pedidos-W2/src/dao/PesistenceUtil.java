/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Lab-01
 */
public class PesistenceUtil {
    
    private static EntityManagerFactory factory;
    
    static {
        factory = Persistence.createEntityManagerFactory("Pedidos-W2PU");
    }
    
    public static EntityManager createEntityManager(){
        return factory.createEntityManager();
    }
    
}
