/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import model.Produto;

/**
 *
 * @author Lab-01
 */
public class ProdutoDAO {
    
    private EntityManager entityManager;
    
    public void salvar (Produto p){
        try{
            entityManager = PesistenceUtil.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(p);
            entityManager.getTransaction().commit();    
        }catch(Exception e){
           entityManager.getTransaction().rollback();
           throw new RuntimeException("Erro ao inserir registro");
        }finally{
            entityManager.close();
        }
    }
    
    public void editar (Produto p){
        try{
            entityManager = PesistenceUtil.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.merge(p);
            entityManager.getTransaction().commit();  
        }catch(Exception e){
            entityManager.getTransaction().rollback();
        }finally{
            entityManager.close();
        }
    }
    
    public void  remover(Produto p){
        try{
            entityManager = PesistenceUtil.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.remove(p);
            entityManager.getTransaction().commit();  
        }catch(Exception e){
            entityManager.getTransaction().rollback();
        }finally{
            entityManager.close();
        }
    }
    
   
    
   
}
